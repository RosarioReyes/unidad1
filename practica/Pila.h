//Header de pila

//Definicion del objeto/Estructura

typedef struct pila pila;

struct pila{
int size;
int index;
int * content;

//Metodos

void (* push)(pila * this,int val);
int (* pop)(pila * this);

};

//Definicion de metodos

void pila_push(pila * this,int val);
int pila_pop(pila * this);

//Constructor

pila * new_pila(int size);

#include <stdlib.h>
#include "pila_impl.c"

int main(){

pila * p = new_pila(10);

p->push(p,11);
p->push(p,7);
p->push(p,35);

printf("El ultimo elemento removido: %d \n",p->pop(p));
printf("El ultimo elemento removido: %d \n",p->pop(p));
printf("El ultimo elemento removido: %d \n",p->pop(p));

getch();
return 0;
}

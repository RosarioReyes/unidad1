/**
* Programa de una pila con datos de tipo entero 
*/
#include "Pila.h"
#include <stdio.h>


/**Constructor, se inicia la pila vac�a 
*/

pila * new_pila(int size){
pila * p;

p = malloc(sizeof(struct pila));
p->content = calloc(size,sizeof(int));

/**Atributos, funciones de la pila*/

p->size=size;
p->index=0;
p->push=pila_push;
p->pop=pila_pop;


return p;
}


/**implementacion de metodos, inicializa la pila inserta el entero val en la pila*/

void pila_push(pila * this,int val){
if(this->index < this->size){

  this->content[this->index] = val;
  this->index++;
}else{

printf("La pila esta llena");

}

}
/**funci�n que recupera el valor entero que se encuentra en el tope de la pila y lo elimina de esta */

int pila_pop(pila * this){
int valor=0;

if(this->index > 0){
    this->index --;
    valor = this->content[this->index];
    this->content[this->index] = 0;
}else{

printf("La Pila esta vacia..\n");
}

return valor;
}
